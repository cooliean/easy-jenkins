package com.easy.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @author 武天
 * @date 2022/12/1 16:14
 */
@Slf4j
public class CustomerFileUtil {

    
    /**
     * 	移动文件到指定位置
     * @param fileFullNameCurrent 要移动的文件全路径
     * @param fileFullNameTarget 移动到目标位置的文件全路径
     * @return 是否移动成功， true：成功；否则失败
     */
    public static Boolean moveFileToTarget(String fileFullNameCurrent,String fileFullNameTarget) 		{
        boolean ismove = false;

        File oldName = new File(fileFullNameCurrent);

        if (!oldName.exists()) {
            log.warn("{}","要移动的文件不存在！");
            return ismove;
        }

        if (oldName.isDirectory()) {
            log.warn("{}","要移动的文件是目录，不移动！");
            return false;
        }

        File newName = new File(fileFullNameTarget);

        if (newName.isDirectory()) {
            log.warn("{}","移动到目标位置的文件是目录，不能移动！");
            return false;
        }

        String pfile = newName.getParent();
        File pdir = new File(pfile);

        if (!pdir.exists()) {
            pdir.mkdirs();
            log.warn("{}","要移动到目标位置文件的父目录不存在，创建：" + pfile);
        }

        ismove = oldName.renameTo(newName);
        return ismove;
    }

    public static void main(String[] args){

        String filePath = "D:\\新建文件夹\\0f7473d732ad5a35acb1d021fe71a782.jenkins";
        String filePathNew = "C:\\\\Users\\\\wutian\\\\Desktop\\0f7473d732ad5a35acb1d021fe71a782.jenkins";

        Boolean ismove = moveFileToTarget(filePath, filePathNew);

        System.out.println(ismove);
        System.out.println("over");
    }

    // 删除某个目录及目录下的所有子目录和文件
    public static boolean deleteDir(File dir) {
        // 如果是文件夹
        if (dir.isDirectory()) {
            // 则读出该文件夹下的的所有文件
            String[] children = dir.list();
            // 递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                // File f=new File（String parent ，String child）
                // parent抽象路径名用于表示目录，child 路径名字符串用于表示目录或文件。
                // 连起来刚好是文件路径
                boolean isDelete = deleteDir(new File(dir, children[i]));
                // 如果删完了，没东西删，isDelete==false的时候，则跳出此时递归
                if (!isDelete) {
                    return false;
                }
            }
        }
        // 读到的是一个文件或者是一个空目录，则可以直接删除
        return dir.delete();
    }

    // 复制某个目录及目录下的所有子目录和文件到新文件夹
    public static void copyFolder(String oldPath, String newPath) {
        try {
            // 如果文件夹不存在，则建立新文件夹
            (new File(newPath)).mkdirs();
            // 读取整个文件夹的内容到file字符串数组，下面设置一个游标i，不停地向下移开始读这个数组
            File filelist = new File(oldPath);
            String[] file = filelist.list();
//            System.out.println(filelist.list(filelist.length()-1));
            // 要注意，这个temp仅仅是一个临时文件指针
            // 整个程序并没有创建临时文件
            File temp = null;
            for (int i = 0; i < file.length; i++) {
                // 如果oldPath以路径分隔符/或者\结尾，那么则oldPath/文件名就可以了
                // 否则要自己oldPath后面补个路径分隔符再加文件名
                // 谁知道你传递过来的参数是f:/a还是f:/a/啊？
                if (oldPath.endsWith(File.separator)) {
                    temp = new File(oldPath + file[i]);
                } else {
                    temp = new File(oldPath + File.separator + file[i]);
                }

                // 如果游标遇到文件
                if (temp.isFile()) {
                    FileInputStream input = new FileInputStream(temp);
                    // 复制并且改名
                    FileOutputStream output = new FileOutputStream(newPath
                            + "/" + (temp.getName()).toString());
                    byte[] bufferarray = new byte[1024 * 64];
                    int prereadlength;
                    while ((prereadlength = input.read(bufferarray)) != -1) {
                        output.write(bufferarray, 0, prereadlength);
                    }
                    output.flush();
                    output.close();
                    input.close();
                }
                // 如果游标遇到文件夹
                if (temp.isDirectory()) {
                    copyFolder(oldPath + "/" + file[i], newPath + "/" + file[i]);
                }
            }
        } catch (Exception e) {
            System.out.println("复制整个文件夹内容操作出错");
        }
    }
}
